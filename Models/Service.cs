using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Text.RegularExpressions;


namespace sopinfra.Models
{
    public class Service
    {
        public List<string> Runserviceprocess(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
                //port = "22";
                string user = "f245277";
               // string pass = "Password";

                ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
                new AuthenticationMethod[]{

                // Basado en autenticacion (usuarion y contraseña)
                //  new PasswordAuthenticationMethod(user, pass),

                // Basado en clave privada
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{

                // Ruta para exportar a contenedor en openshift        
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")

                // Ruta pc local de desarrollo
                //    new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                
                        }),
                     }
                );

            using var client = new SshClient(ConnNfo);
            client.Connect();


            // #####Ejemplos para la sintaxis de comandos##### //

            // Comando SSH
            // client.RunCommand("ls /");

            // Comando powershell basico
            // client.RunCommand("New-Item -Path 'C:/Users/f245277/file.txt' -ItemType File"); 

            // Comando powershell con tuberias
            // var ps = client.RunCommand(
            // "powershell ; " +
            // "\"Get-Process -ComputerName " + host + " | Sort-Object -Property cpu -Descending | Select-Object -First 15 | Format-Table -Property ProcessName,CPU \";");
            var ps = client.RunCommand(
               "powershell.exe ; " +
               "\"Get-Service | select -Property Name\"; ");

            // Correr script remoto de powershell
            // var ps = client.RunCommand("powershell C:/Users/f245277/run.ps1");





            string input = ps.Result;
            List<string> list = new List<string>(
                   input.Split(new string[] { "\r\n" },
                   StringSplitOptions.RemoveEmptyEntries)).ToList();


            client.Disconnect();
            return list;



        }


        public string Statusservicio(string id)
        {
            //string domain = ".corp.ute.com.uy";
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
                "powershell.exe -executionPolicy bypass; " +
                //  "\"Get-Process  | Stop-Process\"; " +
                //  "exit ;");
                //"powershell ; " +
                "\"Get-Service | Format-Table -AutoSize -Property Status,Name,DisplayName\";");
            //  "\"Get-WmiObject -Class Win32_process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property Name,Path,Description \";" +
            // "\"Get-Process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property ProcessName,CPU\"; " +
            //  "exit ;");
            //"\"Get-WmiObject -Class Win32_Share -ComputerName " + host + " | Format-Table -Property Name,Path,Description \";");
            //"\"Get-Process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property ProcessName,CPU \";");
            string input = ps.Result;
            client.Disconnect();
            return (input);
        }




        public string Statuscpu(string id)
        {
            //string domain = ".corp.ute.com.uy";
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            string user = "f245277";                
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
                "powershell.exe -executionPolicy bypass; " +
                //  "\"Get-Process  | Stop-Process\"; " +
                //  "exit ;");
                //"powershell ; " +
                "\"Get-Process | Sort-Object -Property cpu -Descending | Select-Object -First 15 | Format-Table -Property ProcessName,CPU \";");
            //  "\"Get-WmiObject -Class Win32_process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property Name,Path,Description \";" +
            // "\"Get-Process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property ProcessName,CPU\"; " +
            //  "exit ;");
            //"\"Get-WmiObject -Class Win32_Share -ComputerName " + host + " | Format-Table -Property Name,Path,Description \";");
            //"\"Get-Process -ComputerName " + host + " | Select-Object -First 15 | Format-Table -Property ProcessName,CPU \";");
            string input = ps.Result;
            client.Disconnect();
            return (input);
        }

        public string Statusshared(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Get-WmiObject -Class Win32_Share -ComputerName " + host + " | Format-Table -Property Name,Path,Description \";");
            string input = ps.Result;
            client.Disconnect();
            return (input);
        }

        public void Runstopservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Stop-Service -Name \"" + servicio);
            string input = ps.Result;
            client.Disconnect();
        }


        public void Runstartservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Start-Service -Name \"" + servicio);
            string input = ps.Result;
            client.Disconnect();
        }


        public string Runstatusservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Get-Service -Name " + servicio + "  | Format-Table -AutoSize -HideTableHeaders \";");
            string input = ps.Result;
            client.Disconnect();
            return input;



        }


        public string Runstatusweb(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Get-website | Format-Table\";");
            // "\"Get-webapplication | Format-Table\";");
            //"\"Get-Service -Name " + servicio + "  | Format-Table -AutoSize -HideTableHeaders \";") ;
            string input = ps.Result;
            client.Disconnect();
            return input;



        }


    }
}