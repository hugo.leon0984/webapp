﻿using sopinfra.DBContexts;
using sopinfra.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace sopinfra.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class ServerController : Controller
    {
        private readonly MyDBContext myDbContext;


        public ServerController(MyDBContext context)
        {
            myDbContext = context;
        }

        //[HttpGet]
        //public IList<Server> Get()

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Index()
        {
        //    ViewBag.Servers = myDbContext.Servers.ToList();
            //ViewBag.Servers = ctx.Servers.Where(x=>x.Sistema == "Windows").ToList();
            //return RedirectToAction("Home", "Index");
        //    return View();
            return RedirectToAction("Index", "Home");
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult InicioWin()
        {
            ViewBag.Servers = myDbContext.Servers.Where(x => x.Sistema == "Windows").ToList();
            return View();
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult InicioAIX()
        {
            ViewBag.Servers = myDbContext.Servers.Where(x => x.Sistema == "AIX").ToList();
            return View();
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult InicioRed()
        {
            ViewBag.Servers = myDbContext.Servers.Where(x => x.Sistema == "RedHat").ToList();
            return View();
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Create()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([Bind("DnsName,IpAddress,Descripcion,Sistema")] Server server)
        {
            if (ModelState.IsValid)
            {
                myDbContext.Add(server);
                await myDbContext.SaveChangesAsync();
                //return RedirectToAction(nameof(Inicio));
                return RedirectToAction("Index", "Home");
            }
            return View(server);
        }



        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var server = await myDbContext.Servers.FirstOrDefaultAsync(s => s.Id == id);
            if (await TryUpdateModelAsync<Server>(
                server,
                "",
                s => s.DnsName, s => s.IpAddress, s => s.Descripcion, s => s.Sistema))
            {
                try
                {
                    await myDbContext.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(server);
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Edit(int Id)
        {
            var server = myDbContext.Servers.Find(Id);
            if (server == null)
            {
                return Redirect("Index");
            }
            return View(server);
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var server = await myDbContext.Servers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (server == null)
            {
                return NotFound();
            }

            return View(server);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var server = await myDbContext.Servers.FindAsync(id);
            myDbContext.Servers.Remove(server);
            await myDbContext.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Add()
        {
            //    ViewBag.Servers = myDbContext.Servers.ToList();
            //ViewBag.Servers = ctx.Servers.Where(x=>x.Sistema == "Windows").ToList();
            //return RedirectToAction("Home", "Index");
            return View();
           // return RedirectToAction("Index", "Home");
        }

    }
}