﻿using sopinfra.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.Services
{
    public interface IAuthenticationService
    {
        IAppUser Login(string username, string password);
    }
}
